# [doc-gitlab-com](https://doc.gitlab.com/)

Generates html pages that contain the documentation for GitLab based on the doc directory in the GitLab CE repo.

Test with:

```bash
cd /root/doc-gitlab-com && git pull && ./generate.rb no-clone && service nginx restart
```

# Installation


Login as **root** and execute the following commands:

```bash
apt-get update
apt-get upgrade
apt-get install -y git nginx pandoc ruby1.9.1
cd /root
git clone https://gitlab.com/gitlab-com/doc-gitlab-com.git
ln -s /root/doc-gitlab-com/nginx /etc/nginx/sites-available/doc-gitlab-com
ln -s /etc/nginx/sites-available/doc-gitlab-com /etc/nginx/sites-enabled/doc-gitlab-com
rm /etc/nginx/sites-enabled/default
service nginx start
crontab -e
20 */2 * * * /root/doc-gitlab-com/generate.rb
```

Do not forget to add the public ssh key of the server `ssh-keygen` as a deploy key to any non-public repositories in generate.rb.
