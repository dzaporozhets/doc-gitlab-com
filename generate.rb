#!/usr/bin/ruby
# Generates the html documentation from the markdown files in the GitLab repo.
# Call with no-clone to skip the timeconsuming closing process.

puts 'Updating doc.gitlab.com'

require 'fileutils'

repos = {
  'ce' => 'https://gitlab.com/gitlab-org/gitlab-ce.git',
  'ee' => 'git@gitlab.com:subscribers/gitlab-ee.git',
  'ci' => 'https://gitlab.com/gitlab-org/gitlab-ci.git'
}
repo_path = '/root/doc-gitlab-com'
site_path = '/srv/doc-gitlab-com'

repos.each do |relative_path, clone_url|

  tmp_dir = File.join('/tmp/gitlab', relative_path)

  unless 'no-clone' == ARGV[0]
    system("rm -rf #{tmp_dir}")
    system("git clone #{clone_url} #{tmp_dir}")
  end

  full_doc_directories = Dir.glob "#{tmp_dir}/doc/*"
  doc_directories = full_doc_directories.map { |f| f.gsub(/#{tmp_dir}\/doc\//, '') }

  all_directories = doc_directories.unshift('') # Also add the root README.md, must be the first one.

  print "Generating pages for #{relative_path}: "
  all_directories.each do |directory|
    destination_dir = [site_path, relative_path, directory].join('/')
    FileUtils.rm_rf(destination_dir) if File.exist?(destination_dir)
    FileUtils.mkdir_p(destination_dir)
    path = File.join(tmp_dir, 'doc', directory, "*.md")

    Dir[path].each do |markdown_file|
      base_html = File.read(File.join(repo_path, 'sample.html'))
      html = `pandoc #{markdown_file}`  

      html.gsub!(/\.md/, '.html')
      html = base_html.gsub(/MCONTENT/, html)
      html = html.gsub(/TIMENOW/, Time.now.asctime)
      html = html.gsub(/MREPOLINK/, clone_url.gsub('.git', ''))

      filename = File.basename(markdown_file).gsub('.md', '.html')
      File.open(File.join(site_path, relative_path, directory, filename), 'w') {
        |file| file.write(html)
      }

      print '.'
    end
  end
  puts '' # Create a newline
end

puts 'Generating stylesheets'
repo_stylesheets_path = File.join(repo_path, 'stylesheets')
site_stylesheets_path = File.join(site_path, 'stylesheets')
FileUtils.rm_rf(site_stylesheets_path) if File.exist?(site_stylesheets_path)
FileUtils.cp_r(repo_stylesheets_path, site_stylesheets_path)

puts 'Generating index'
repo_index_path = File.join(repo_path, 'index.html')
site_index_path = File.join(site_path, 'index.html')
File.delete(site_index_path) if File.exist?(site_index_path)
FileUtils.cp(repo_index_path, site_index_path)

puts 'Done'
